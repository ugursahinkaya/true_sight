﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed = 10.0f;
    public float jumpHeight = 5.0f;
    public GameObject fpsCamera;

    private bool isCrouching = false;
    private bool isOnGround = true;
    private Rigidbody rigidBody;
    private float gravity = 10;
    private CameraControl cameraScript;

    // Light Vars
    public Light playerLight;
    private Color[] lightModes = { Color.blue / 2, Color.red / 2, Color.green / 2 };
    private int mode;

    //Level Transition Vars
    public GameObject levelChanger;
    private LevelChanger lvChangeScript;

    //Blue Tile Flags
    private bool canGoRight = true;
    private bool canGoLeft = true;
    private bool canGoBack = true;
    private bool canGoForward = true;
    private bool onBlue = false;
    private bool onBlueGround = false;
    private bool canSwitch = false;
    private GameObject toSwitch = null;

    // Use this for initialization
    void Start()
    {
        //light
        mode = 0;
        playerLight.color = lightModes[mode];

        //physics
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;

        //cursor
        Cursor.lockState = CursorLockMode.Locked;

        //fading
        lvChangeScript = levelChanger.GetComponent<LevelChanger>();

        //getting camera script
        cameraScript = fpsCamera.GetComponent<CameraControl>();
        cameraScript.SetState(this.transform);
    }

    private void FixedUpdate()
    {
        // apply constant weight force according to character normal:
        rigidBody.AddForce(-gravity * rigidBody.mass * this.transform.up);

        Vector3 physicsCentre = this.transform.position + this.GetComponent<CapsuleCollider>().center;

        //Movement restriction for blue tiles
        RaycastHit rightHit;
        RaycastHit leftHit;
        RaycastHit forwardHit;
        RaycastHit backHit;
        Vector3 rightRay = (this.transform.right * 0.3f) - this.transform.up;
        Vector3 leftRay = (this.transform.right * -0.3f) - this.transform.up;
        Vector3 forwardRay = (this.transform.forward * 0.3f) - this.transform.up;
        Vector3 backRay = (this.transform.forward * -0.3f) - this.transform.up;


        if (Physics.Raycast(physicsCentre, rightRay, out rightHit, 1.5f))
        {
            //Debug.Log(rightHit.transform.gameObject.tag);
            if (rightHit.transform.gameObject.tag != "Blue" && rightHit.transform.gameObject.tag != "Red Tile")
            {
                canGoRight = false;
            }
            else
            {
                canGoRight = true;
            }
        }
        else
        {
            //Debug.Log("right else");
            canGoRight = false;
        }

        if (Physics.Raycast(physicsCentre, leftRay, out leftHit, 1.5f))
        {
            if (leftHit.transform.gameObject.tag != "Blue" && leftHit.transform.gameObject.tag != "Red Tile")
            {
                canGoLeft = false;
            }
            else
            {
                canGoLeft = true;
            }
        }
        else
        {
            canGoLeft = false;
        }

        if (Physics.Raycast(physicsCentre, forwardRay, out forwardHit, 1.5f))
        {
            if (forwardHit.transform.gameObject.tag != "Blue" && forwardHit.transform.gameObject.tag != "Red Tile")
            {
                canGoForward = false;
            }
            else
            {
                canGoForward = true;
            }
        }
        else
        {
            canGoForward = false;
        }

        if (Physics.Raycast(physicsCentre, backRay, out backHit, 1.5f))
        {
            if (backHit.transform.gameObject.tag != "Blue" && backHit.transform.gameObject.tag != "Red Tile")
            {
                canGoBack = false;
            }
            else
            {
                canGoBack = true;
            }
        }
        else
        {
            canGoBack = false;
        }

        //ground detection
        RaycastHit downHit;
        if (Physics.Raycast(physicsCentre, -this.transform.up, out downHit, 1.25f))
        {
            if (downHit.transform.gameObject.tag != "Player")
            {
                isOnGround = true;
            }

            if (downHit.transform.gameObject.tag == "Blue")
            {
                onBlue = true;
            }
            else
            {
                onBlue = false;
            }

            if (downHit.transform.gameObject.tag == "Red Tile")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            if (downHit.transform.gameObject.tag == "Blue Ground")
            {
                onBlueGround = true;
            }
            else
            {
                onBlueGround = false;
            }
        }
        else
        {
            isOnGround = false;
            onBlue = false;
            onBlueGround = false;
        }

        //Switching to Walls
        RaycastHit blueHit;
        if (Physics.Raycast(physicsCentre, this.transform.forward, out blueHit, 1.25f))
        {
            if (blueHit.transform.gameObject.tag == "Blue" || blueHit.transform.gameObject.tag == "Blue Ground")
            {
                canSwitch = true;
                toSwitch = blueHit.transform.gameObject;
            }
            else
            {
                canSwitch = false;
            }
        }
        else
        {
            canSwitch = false;
        }
    }

    // Update is called once per frame
    void Update()
    {


        //Movement && application of blue limitation
        float forward = Input.GetAxis("Vertical") * speed;
        float side = Input.GetAxis("Horizontal") * speed;

        if ((onBlueGround || onBlue) &&
            canSwitch &&
            (forward > 0)) //Problem Point 2
        {
            this.transform.up = toSwitch.transform.up;
            cameraScript.SetState(this.transform);            
        }
        else
        {
            cameraScript.UpdatePlayerRotation();
        }

        if (!onBlue)
        {
            forward *= Time.deltaTime;
            side *= Time.deltaTime;
        }
        else if (onBlue)
        {
            if (!canGoLeft)
            {
                side = Mathf.Clamp(side, 0f, speed);
            }
            if (!canGoRight)
            {
                side = Mathf.Clamp(side, -speed, 0f);
            }
            if (!canGoBack)
            {
                forward = Mathf.Clamp(forward, 0f, speed);
            }
            if (!canGoForward)
            {
                forward = Mathf.Clamp(forward, -speed, 0f);
            }

            forward *= Time.deltaTime;
            side *= Time.deltaTime;
        }

        transform.Translate(side, 0, forward);

        //Extra Input
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            SwitchMode();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!isCrouching && isOnGround)
            {
                transform.position -= this.transform.up * 0.5f;
                transform.localScale -= new Vector3(0, 0.5f, 0);
                speed = 5.0f;
                isCrouching = true;
            }
            else if (isCrouching)
            {
                transform.position += this.transform.up * 0.5f;
                transform.localScale += new Vector3(0, 0.5f, 0);
                speed = 10.0f;
                isCrouching = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && isOnGround && !onBlue)
        {
            if (isCrouching)
            {
                transform.position += this.transform.up * 0.5f;
                transform.localScale += new Vector3(0, 0.5f, 0);
                speed = 10.0f;
                isCrouching = false;
            }
            rigidBody.velocity = this.transform.up * jumpHeight;
        }
        //Debug.Log(onBlue);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Harmful"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (other.tag.Equals("Green"))
        {
            lvChangeScript.FadetoNextLevel();
        }
    }

    private void SwitchMode()
    {
        mode = (mode + 1) % 3;
        playerLight.color = lightModes[mode];
    }

    public int getMode()
    {
        return mode;
    }

    void OnDrawGizmos()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = (this.transform.right * 0.3f) - this.transform.up;
        Gizmos.DrawRay(transform.position, direction * 2);

        Gizmos.color = Color.blue;
        direction = (this.transform.right * -0.3f) - this.transform.up;
        Gizmos.DrawRay(transform.position, direction * 2);

        Gizmos.color = Color.yellow;
        direction = (this.transform.forward * 0.3f) - this.transform.up;
        Gizmos.DrawRay(transform.position, direction * 2);

        Gizmos.color = Color.green;
        direction = (this.transform.forward * -0.3f) - this.transform.up;
        Gizmos.DrawRay(transform.position, direction * 2);

        Gizmos.color = Color.cyan;
        direction = this.transform.forward;
        Gizmos.DrawRay(transform.position, direction * 1.25f);

        Gizmos.color = Color.magenta;
        direction = -this.transform.up;
        Gizmos.DrawRay(transform.position, direction * 1.5f);
    }
}


/* Other tasks
 * 
 * Anti gravity walking
 * Checkpoints
 * Level Design
 * 
 * END
 */
