﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CameraControl : MonoBehaviour {
     /*public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
     public RotationAxes axes = RotationAxes.MouseXAndY;
     public float sensitivityX = 15F;
     public float sensitivityY = 15F;
     public float minimumX = -360F;
     public float maximumX = 360F;
     public float minimumY = -60F;
     public float maximumY = 60F;
     float rotationX = 0F;
     float rotationY = 0F;
     GameObject player;
     private List<float> rotArrayX = new List<float>();
     float rotAverageX = 0F;
     private List<float> rotArrayY = new List<float>();
     float rotAverageY = 0F;
     public float frameCounter = 20;
     Quaternion originalRotation;
     void Update()
     {
         if (axes == RotationAxes.MouseXAndY)
         {
             //Resets the average rotation
             rotAverageY = 0f;
             rotAverageX = 0f;

             //Gets rotational input from the mouse
             rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
             rotationX += Input.GetAxis("Mouse X") * sensitivityX;

             //Adds the rotation values to their relative array
             rotArrayY.Add(rotationY);
             rotArrayX.Add(rotationX);

             //If the arrays length is bigger or equal to the value of frameCounter remove the first value in the array
             if (rotArrayY.Count >= frameCounter)
             {
                 rotArrayY.RemoveAt(0);
             }
             if (rotArrayX.Count >= frameCounter)
             {
                 rotArrayX.RemoveAt(0);
             }

             //Adding up all the rotational input values from each array
             for (int j = 0; j < rotArrayY.Count; j++)
             {
                 rotAverageY += rotArrayY[j];
             }
             for (int i = 0; i < rotArrayX.Count; i++)
             {
                 rotAverageX += rotArrayX[i];
             }

             //Standard maths to find the average
             rotAverageY /= rotArrayY.Count;
             rotAverageX /= rotArrayX.Count;

             //Clamp the rotation average to be within a specific value range
             rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
             rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

             //Get the rotation you will be at next as a Quaternion
             Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
             Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);

             //Rotate
             Quaternion rotation = originalRotation * xQuaternion * yQuaternion;
            transform.localRotation = rotation;
         }
         else if (axes == RotationAxes.MouseX)
         {
             rotAverageX = 0f;
             rotationX += Input.GetAxis("Mouse X") * sensitivityX;
             rotArrayX.Add(rotationX);
             if (rotArrayX.Count >= frameCounter)
             {
                 rotArrayX.RemoveAt(0);
             }
             for (int i = 0; i < rotArrayX.Count; i++)
             {
                 rotAverageX += rotArrayX[i];
             }
             rotAverageX /= rotArrayX.Count;
             rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);
             Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
             transform.localRotation = originalRotation * xQuaternion;
         }
         else
         {
             rotAverageY = 0f;
             rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
             rotArrayY.Add(rotationY);
             if (rotArrayY.Count >= frameCounter)
             {
                 rotArrayY.RemoveAt(0);
             }
             for (int j = 0; j < rotArrayY.Count; j++)
             {
                 rotAverageY += rotArrayY[j];
             }
             rotAverageY /= rotArrayY.Count;
             rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
             Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
             transform.localRotation = originalRotation * yQuaternion;
         }
     }
     void Start()
     {
         player = this.transform.parent.gameObject;
         Rigidbody rb = GetComponent<Rigidbody>();
         if (rb)
             rb.freezeRotation = true;
         originalRotation = transform.localRotation;
     }
     public static float ClampAngle(float angle, float min, float max)
     {
         angle = angle % 360;
         if ((angle >= -360F) && (angle <= 360F))
         {
             if (angle < -360F)
             {
                 angle += 360F;
             }
             if (angle > 360F)
             {
                 angle -= 360F;
             }
         }
         return Mathf.Clamp(angle, min, max);
     }
     */
    Vector2 mouseLook = new Vector2(0, 0);
    Vector2 smoothLook;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;
    private enum upVector { isX = 0, isY = 1, isZ = 2, isMinusX = 3, isMinusY = 4, isMinusZ = 5 };
    private upVector state;
    private Vector3 baseEulerAngles;
    GameObject player;
    // Use this for initialization
    void Start () {
        player = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update () {

        Vector2 direction = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        smoothLook.x = Mathf.Lerp(smoothLook.x, direction.x, 1.0f / smoothing);
        smoothLook.y = Mathf.Lerp(smoothLook.y, direction.y, 1.0f / smoothing);
        mouseLook += smoothLook;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

        UpdateVerticalRotation();
    }
    
    private void UpdateVerticalRotation()
    {
        //Works on every surface
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
    }

    public void UpdatePlayerRotation() //Will be called by player for addressing syncronization issues
    {
        if (state == upVector.isX)
        {
            player.transform.eulerAngles = baseEulerAngles + new Vector3(mouseLook.x, 0, 0);
        }
        else if (state == upVector.isY)
        {
            player.transform.eulerAngles = baseEulerAngles + new Vector3(0, mouseLook.x, 0);
        }
        else if (state == upVector.isZ)
        {
            player.transform.eulerAngles = baseEulerAngles + new Vector3(0, 0, mouseLook.x);
        }
        else if (state == upVector.isMinusX)
        {
            player.transform.eulerAngles = baseEulerAngles + new Vector3(-mouseLook.x, 0, 0);
        }
        else if (state == upVector.isMinusY)
        {
            player.transform.eulerAngles = baseEulerAngles + new Vector3(0, -mouseLook.x + 180, 0);
        }
        else if (state == upVector.isMinusZ)
        {
            player.transform.eulerAngles = baseEulerAngles;
            player.transform.eulerAngles.z += -mouseLook.x;
        }
    }

    public void SetState(Transform playerTransform)
    {
        baseEulerAngles = transform.eulerAngles;
        RoundAngles(ref baseEulerAngles);

        if(playerTransform.up == Vector3.right)
        {
            state = upVector.isX;
        }
        else if (playerTransform.up == Vector3.up)
        {
            state = upVector.isY;
        }
        else if(playerTransform.up == Vector3.forward)
        {
            state = upVector.isZ;
        }
        else if (playerTransform.up == -Vector3.right)
        {
            state = upVector.isMinusX;
        }
        else if(playerTransform.up == -Vector3.up)
        {
            state = upVector.isMinusY;
        }
        else if(playerTransform.up == -Vector3.forward)
        {
            state = upVector.isMinusZ;
        }
        else
        {
            Debug.Log("Undefined State: " + playerTransform.up);
        }
    }

    private void RoundAngles(ref Vector3 baseAngles)
    {
        baseAngles = new Vector3(roundNinety(baseAngles.x),
                                 roundNinety(baseAngles.y),
                                 roundNinety(baseAngles.z));
    }

    private float roundNinety(float degree)
    {
        if(degree >= 335f || degree <= 25f)
        {
            return 0f;    
        }
        else if(degree >= 65f && degree <= 115f)
        {
            return 90f;
        }
        else if (degree >= 155f && degree <= 205)
        {
            return 180f;
        }
        else if (degree >= 245f && degree <= 295f)
        {
            return 270f;
        }
        else
        {
            return degree;
        }
    }
}
