﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedManager : MonoBehaviour {
    
    public GameObject player;

    private Player playerScript;
    private GameObject red;
    
    // Use this for initialization
    void Start()
    {
        playerScript = player.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        int mode = playerScript.getMode();

        if (mode == 1) // If light is red
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Show()
    {
        Component[] children = GetComponentsInChildren(typeof(Renderer));
        foreach (Component child in children)
        {
            Renderer childRend = (Renderer)child;
            childRend.enabled = true;
        }
    }

    private void Hide()
    {
        Component[] children = GetComponentsInChildren(typeof(Renderer));
        foreach (Component child in children)
        {
            Renderer childRend = (Renderer)child;
            childRend.enabled = false;
        }
    }
}
