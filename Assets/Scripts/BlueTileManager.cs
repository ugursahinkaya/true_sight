﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueTileManager : MonoBehaviour {

    public Material[] mats;
    public GameObject player;

    private Player playerScript;
    private Renderer rend;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = mats[0];
        playerScript = player.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        int mode = playerScript.getMode();

        if (mode == 0) // If light is blue
        {
            rend.sharedMaterial = mats[1];
        }
        else
        {
            rend.sharedMaterial = mats[0];
        }
    }
}
